package common;

/**
 * User: Oleg Larionov
 * Date: 30.04.13
 * Time: 0:59
 */
public class Query {
    private String query;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
