<%@ page import="org.apache.lucene.queryparser.classic.ParseException" %>
<%@ page import="org.apache.lucene.search.ScoreDoc" %>
<%@ page import="ru.ifmo.ctddev.search.exceptions.SearchRuntimeException" %>
<%@ page import="ru.ifmo.ctddev.search.index.DocumentField" %>
<%@ page import="ru.ifmo.ctddev.search.index.Searcher" %>
<%--
  User: Oleg Larionov, Vladislav Kononov
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="query" class="common.Query" scope="session"/>
<jsp:setProperty name="query" property="*"/>
<%!
    private String normalize(String q) {
        if (q != null) {
            q = q.trim();
        }
        if (q != null) {
            q = q.toLowerCase();
        }

        return q;
    }
%>

<%
    Searcher searcher = new Searcher();
    String q = query.getQuery();
    q = normalize(q);
%>

<html>
<head>
    <title>Results</title>
    <link href="style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<jsp:include page="index.jsp"/>
<%
    if (q == null || q.isEmpty()) {
        out.println("Query must not be empty!");
    } else {
        ScoreDoc[] results = {};
        try {
            results = searcher.search(q);
        } catch (ParseException e) {
            pageContext.forward("error.jsp");
        } catch (SearchRuntimeException e) {
            pageContext.forward("error.jsp");
        }
        if (results.length == 0) {
            out.println("No results! Try other query.");
        } else {
%>
<table id="results">

    <tr>
        <th>
            Number
        </th>
        <th>
            Link
        </th>
    </tr>

    <%
        int i = 0;
        for (ScoreDoc scoreDoc : results) {
    %>

    <tr <% if (i % 2 == 1) out.println("class=\"alt\""); %> >
        <td><%= i++ %>
        </td>
        <td>
            <%
                String result = searcher.searchByDocId(scoreDoc.doc).get(DocumentField.URL.toString());
                if (!result.contains("http://")) {
                    result = "http://" + result;
                }
                out.println(String.format("<a href=%s>%s</a>", result, result));
            %>
        </td>
    </tr>
    <%
                }
            }
        }
    %>
</table>
</body>
</html>