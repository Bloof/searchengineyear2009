<%--
  User: Oleg Larionov
  Date: 04.05.13
  Time: 22:57
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error!</title>
</head>
<body>
<jsp:include page="index.jsp"/>
Your request contains errors! Please, try other query.
</body>
</html>