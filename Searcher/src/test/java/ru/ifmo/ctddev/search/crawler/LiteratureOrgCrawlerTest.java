package ru.ifmo.ctddev.search.crawler;

import edu.uci.ics.crawler4j.url.WebURL;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * User: Oleg Larionov
 * Date: 28.04.13
 * Time: 20:52
 */
@RunWith(MockitoJUnitRunner.class)
public class LiteratureOrgCrawlerTest {

    @Mock
    private File file;
    private LiteratureOrgCrawler crawler;

    @Before
    public void setUp() {
        when(file.canRead()).thenReturn(true);
        when(file.canWrite()).thenReturn(true);
        when(file.isDirectory()).thenReturn(true);
        when(file.getAbsolutePath()).thenReturn("D:/path/to/dir");

        crawler = new LiteratureOrgCrawler();
        crawler.setMainDirectory(file);
    }

    @Test
    public void testShouldVisitPositive() {
        WebURL url = getUrl("http://www.literature.org/authors/");
        WebURL urlTextPage = getUrl("http://www.literature.org/authors/tolstoy-leo/war-and-peace/part-01/chapter-26.html");

        assertTrue(crawler.shouldVisit(url));
        assertTrue(crawler.shouldVisit(urlTextPage));
    }

    @Test
    public void testShouldVisitGoogleCom() throws Exception {
        WebURL urlGoogleCom = getUrl("http://google.com/");
        WebURL urlGoogleComWithGoodExtension = getUrl("http://google.com/test.html");

        assertFalse(crawler.shouldVisit(urlGoogleCom));
        assertFalse(crawler.shouldVisit(urlGoogleComWithGoodExtension));
    }

    private WebURL getUrl(String ref) {
        WebURL url = new WebURL();
        url.setURL(ref);

        return url;
    }
}
