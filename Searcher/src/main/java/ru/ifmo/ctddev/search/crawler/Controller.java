package ru.ifmo.ctddev.search.crawler;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import ru.ifmo.ctddev.search.Settings;
import ru.ifmo.ctddev.search.exceptions.SearchRuntimeException;

/**
 * Контроллер для запуска пауков. Свою информацию хранит в папке "crawl/".
 * Можно варьировать количество пауков. При обходе использует {@link LiteratureOrgCrawler}
 * <p/>
 * User: Oleg Larionov
 * Date: 28.04.13
 * Time: 21:17
 */
public class Controller implements Runnable {

    public static final String CRAWL_FOLDER = "run/crawl/";
    public static final int REQUEST_DELAY = 1000;
    /**
     * Дефолтное значение для количества пауков.
     */
    private int numberOfCrawlers = 2;

    public Controller() {
    }

    public Controller(int numberOfCrawlers) {
        if (numberOfCrawlers <= 0) {
            throw new IllegalArgumentException("numberOfCrawler must be > 0");
        }

        this.numberOfCrawlers = numberOfCrawlers;
    }

    @Override
    public void run() {
        if (Settings.getInstance().getIndexer() == null) {
            throw new SearchRuntimeException("Indexer in Settings must not be null!");
        }

        CrawlController controller = createDefaultCrawlController();
        for (String seed : TheFreeLibraryCrawler.getSeeds()) {
            controller.addSeed(seed);
        }

        try {
            controller.start(TheFreeLibraryCrawler.class, numberOfCrawlers);
        } finally {
            Settings.getInstance().getIndexer().closeIndexWriter();
        }
    }

    private CrawlController createDefaultCrawlController() {
        CrawlConfig config = new CrawlConfig();
        config.setCrawlStorageFolder(CRAWL_FOLDER);
        config.setPolitenessDelay(REQUEST_DELAY);
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller;
        try {
            controller = new CrawlController(config, pageFetcher, robotstxtServer);
            return controller;
        } catch (Exception e) {
            throw new SearchRuntimeException("Cannot create CrawlController", e);
        }
    }
}
