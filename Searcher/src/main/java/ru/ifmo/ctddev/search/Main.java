package ru.ifmo.ctddev.search;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.ScoreDoc;
import ru.ifmo.ctddev.search.crawler.Controller;
import static ru.ifmo.ctddev.search.crawler.JerryCrawler.PATH;
import ru.ifmo.ctddev.search.exceptions.SearchRuntimeException;
import ru.ifmo.ctddev.search.index.DocumentField;
import ru.ifmo.ctddev.search.index.Indexer;
import ru.ifmo.ctddev.search.index.Searcher;

import java.io.*;
import java.util.Scanner;

public class Main {
    private static final String CONFIG_FILENAME = "resources/log4j.properties";

    static {
        PropertyConfigurator.configure(CONFIG_FILENAME);
    }

    //private static Logger logger = Logger.getLogger(Main.class);

    /**
     * Запускатель движка. Первым параметром нужно указать, что мы хотим делать:
     * либо построение индекса (слово "crawl"), либо поиск по построенному индексу ("search").
     * Единовременно можно выполнять либо кроулинг, либо поиск.
     * Индекс сохраняется в папке, таким образом его можно накапливать и передавать.
     *
     * @param args первым параметром команда
     */
    public static void main(String[] args) {
        System.out.println("Hello, World!");

        switch (args[0]) {
            case "test":
                Indexer indexer = new Indexer();
                File file = new File("C:/index/temp/");
                for (File f : file.listFiles()){
                    try {
                        BufferedReader reader = new BufferedReader(new FileReader(f));
                        StringBuilder wholeText = new StringBuilder();
                        String url = reader.readLine();
                        String temp;
                        while ((temp = reader.readLine()) != null){
                            wholeText.append(temp);
                        }
                        reader.close();
                        System.out.println(url);
                        indexer.addBook(url, wholeText.toString());
                    } catch (FileNotFoundException e) {
                        System.out.println("Can not find " + f.getName());
                    } catch (IOException e) {
                        System.out.println("Can not read from " + f.getName());
                    }
                }
                indexer.closeIndexWriter();
                break;
            case "crawl":
                System.out.println("Crawling started!");
                Settings.getInstance().setIndexer(new Indexer());
                Thread controller = new Thread(new Controller());
                controller.start();
                System.out.println("Waiting for end crawling...");
                synchronized (controller) {
                    try {
                        controller.wait();
                    } catch (InterruptedException ignored) {
                    }
                }
                System.out.println("Done");
                break;
            case "search":
                System.out.println("Preparing to search...");
                Searcher searcher = new Searcher();
                System.out.println("Ready for requests...");
                String query;
                try (Scanner sc = new Scanner(System.in)) {
                    while (!(query = sc.nextLine().trim()).equalsIgnoreCase("q")) {
                        try {
                            ScoreDoc[] found = searcher.search(query);
                            for (ScoreDoc document : found) {
                                Document doc = searcher.searchByDocId(document.doc);
                                System.out.println(doc.get(DocumentField.URL.toString()));
                            }
                        } catch (ParseException e) {
                            System.err.println("Cannot parse request!");
                        } catch (SearchRuntimeException e) {
                            e.printStackTrace();
                        }
                    }
                }
                System.out.println("Done");
                break;
            default:
                System.out.println("Usage: \"crawl\" or \"search\"");
                break;
        }
    }

    /*
     *  Примеры использования паука и индексатора.
     */
    public static void crawlExample() {
        Thread crawlThread = new Thread(new Controller());
        crawlThread.start();
    }

    public static void indexExample() {
        initIndex();
        Searcher searcher = new Searcher();
        try {
            ScoreDoc[] found = searcher.search("voluptate");
            for (ScoreDoc document : found) {
                Document doc = searcher.searchByDocId(document.doc);
                System.out.println(doc.get(DocumentField.URL.toString()));
            }
        } catch (ParseException e) {
            System.err.println("Cannot parse query");
        }
    }

    private static void initIndex() {
        Indexer indexer = new Indexer();
        indexer.addBook("http://www.google.com/", "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore");
        indexer.addBook("http://www.yandex.ru/", "Lorem ipsum dolor sit amet consectetur adipisicing elit");
        indexer.addBook("http://www.rambler.ru/", "in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident");
        indexer.closeIndexWriter();
    }
}
