package ru.ifmo.ctddev.search.index;

/**
 * User: Oleg Larionov
 * Date: 29.04.13
 * Time: 13:00
 */
public enum DocumentField {
    TEXT("text"), URL("url");
    private String toString;

    private DocumentField(String toString) {
        this.toString = toString;
    }

    @Override
    public String toString() {
        return toString;
    }
}
