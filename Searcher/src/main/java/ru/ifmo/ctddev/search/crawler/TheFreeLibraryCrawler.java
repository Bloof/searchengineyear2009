package ru.ifmo.ctddev.search.crawler;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.log4j.Logger;
import ru.ifmo.ctddev.search.Settings;
import ru.ifmo.ctddev.search.index.Indexer;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * User: Oleg Larionov
 * Date: 03.05.13
 * Time: 22:17
 */
public class TheFreeLibraryCrawler extends WebCrawler {

    public static final Pattern THE_FREE_LIBRARY_PATTERN = Pattern.compile("^http://.+\\.thefreelibrary\\.com.*");
    public static final Pattern IGNORE_WWW = Pattern.compile("^http://www.*");
    public static final Pattern IGNORED_FORMATS = Pattern.compile(".*\\.(css|js|bmp|gif|jpe?g" +
            "|png|tiff?|mid|mp2|mp3|mp4" +
            "|wav|avi|mov|mpeg|ram|m4v|pdf|aspx" +
            "|rm|smil|wmv|swf|wma|zip|rar|gz|xml)(\\?.*)?$");
    public static final Pattern PAGE_403 = Pattern.compile(".*_/access/403.aspx$");
    private static Logger logger = Logger.getLogger(TheFreeLibraryCrawler.class);
    private Indexer indexer;

    public TheFreeLibraryCrawler() {
        indexer = Settings.getInstance().getIndexer();
    }

    public static List<String> getSeeds() {
        List<String> seeds = new ArrayList<>();
        for (char c = 'A'; c <= 'Z'; ++c) {
            seeds.add("http://www.thefreelibrary.com/ti.aspx?" + c);
        }

        return seeds;
    }

    @Override
    public boolean shouldVisit(WebURL url) {
        String ref = url.getURL().toLowerCase();
        boolean should = THE_FREE_LIBRARY_PATTERN.matcher(ref).matches() && !IGNORE_WWW.matcher(ref).matches()
                && !PAGE_403.matcher(ref).matches();
        logger.info(should + " shouldVisit: " + ref);
        return should;
    }

    @Override
    public void visit(Page page) {
        String url = page.getWebURL().getURL();
        logger.info("visit url: " + url);

        if (!IGNORED_FORMATS.matcher(url).matches()) {
            if (page.getParseData() instanceof HtmlParseData) {
                HtmlParseData htmlData = (HtmlParseData) page.getParseData();
                String text = htmlData.getText().toLowerCase();
                if (!text.contains("access denied")) {
                    addPageToIndex(url, text);
                }
            }
        }
    }

    private void addPageToIndex(String url, String text) {
        indexer.addBook(url, text);
    }
}
