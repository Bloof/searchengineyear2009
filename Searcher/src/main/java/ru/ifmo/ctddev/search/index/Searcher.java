package ru.ifmo.ctddev.search.index;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.DefaultSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import ru.ifmo.ctddev.search.exceptions.SearchRuntimeException;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * User: Влад, Oleg Larionov
 * Date: 29.04.13
 * Time: 4:17
 */
public class Searcher {

    private static final int DEFAULT_RESULT_NUMBER = 30;
    private static Logger logger = Logger.getLogger(Searcher.class);
    private IndexSearcher searcher;
    private QueryParser queryParser;
    private int resultNumber;

    public Searcher() {
        this(Indexer.INDEX_FOLDER, DEFAULT_RESULT_NUMBER);
    }

    public Searcher(String indexPath, int resultNumber) {
        this.resultNumber = resultNumber;
        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_42);
        queryParser = new QueryParser(Version.LUCENE_42, DocumentField.TEXT.toString(), analyzer);
        queryParser.setDefaultOperator(QueryParser.Operator.AND);
        try {
            indexPath = getClasspathPrefix() + indexPath;
            Directory directory = FSDirectory.open(new File(indexPath));
            DirectoryReader reader = DirectoryReader.open(directory);
            searcher = new IndexSearcher(reader);
            searcher.setSimilarity(new DefaultSimilarity() {
                @Override
                public float coord(int overlap, int maxOverlap) {
                    return 1.0f;
                }

                @Override
                public float queryNorm(float sumOfSquarredWeights) {
                    return 1.0f;
                }

                @Override
                public float lengthNorm(org.apache.lucene.index.FieldInvertState state) {
                    return 1.0f;
                }
            });
        } catch (IOException e) {
            throw new SearchRuntimeException("Cannot open index directory " + indexPath, e);
        }
    }

    private String getClasspathPrefix() {
        URL testFile = Thread.currentThread().getContextClassLoader().getResource("test.txt");
        String pathToTestFile = testFile.getFile().replace("%20", " ");
        String classPath = pathToTestFile.substring(0, pathToTestFile.lastIndexOf("/") + 1);
        return classPath;
    }

    public ScoreDoc[] search(String query) throws ParseException {
        logger.info("Search request: " + query);

        Query q = queryParser.parse(query);
        TopScoreDocCollector collector = TopScoreDocCollector.create(resultNumber, true);

        try {
            searcher.search(q, collector);
            ScoreDoc[] docs = collector.topDocs().scoreDocs;
            for (ScoreDoc doc : docs) {
                logger.info(searcher.explain(q, doc.doc));
            }
            return docs;
        } catch (IOException e) {
            throw new SearchRuntimeException("Cannot ru.ifmo.ctddev.search in index", e);
        }
    }

    public Document searchByDocId(int docId) {
        logger.info("Search docId request: " + docId);
        try {
            Document found = searcher.doc(docId);
            return found;
        } catch (IOException e) {
            throw new SearchRuntimeException(
                    String.format("Cannot ru.ifmo.ctddev.search document %d in index", docId), e);
        }
    }
}
