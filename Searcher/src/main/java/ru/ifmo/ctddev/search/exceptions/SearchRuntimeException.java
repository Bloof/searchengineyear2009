package ru.ifmo.ctddev.search.exceptions;

/**
 * Непроверяемое исключение для ошибок приложения.
 * <p/>
 * User: Oleg Larionov
 * Date: 28.04.13
 * Time: 21:35
 */
public class SearchRuntimeException extends RuntimeException {
    public SearchRuntimeException(String message) {
        super(message);
    }

    public SearchRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
