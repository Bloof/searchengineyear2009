package ru.ifmo.ctddev.search.index;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import ru.ifmo.ctddev.search.exceptions.SearchRuntimeException;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

/**
 * User: Влад
 * Date: 29.04.13
 * Time: 4:05
 */
public class Indexer {

    public static final String INDEX_FOLDER = "run/index/";
    public static final int MODULO = 100;
    private static Logger logger = Logger.getLogger(Indexer.class);
    private IndexWriter indexWriter;
    private AtomicLong counter;

    public Indexer() {
        this(INDEX_FOLDER);
    }

    public Indexer(String indexPath) {
        indexWriter = createIndexWriter(indexPath);
        counter = new AtomicLong();
    }

    private IndexWriter createIndexWriter(String indexPath) {
        logger.info("Creating IndexWriter to " + indexPath);

        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_42);
        IndexWriterConfig conf = new IndexWriterConfig(Version.LUCENE_42, analyzer);
        conf.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        Directory directory;
        try {
            File path = new File(indexPath);
            path.mkdirs();
            directory = FSDirectory.open(path);
        } catch (IOException e) {
            throw new SearchRuntimeException("Cannot open directory " + indexPath, e);
        }
        try {
            return new IndexWriter(directory, conf);
        } catch (IOException e) {
            throw new SearchRuntimeException("Cannot create IndexWriter", e);
        }
    }

    public void addBook(String url, String text) {
        logger.info("Adding book to index: " + url);

        Document doc = new Document();
        doc.add(new TextField(DocumentField.TEXT.toString(), text, Field.Store.NO));
        doc.add(new StringField(DocumentField.URL.toString(), url, Field.Store.YES));
        try {
            indexWriter.addDocument(doc);
            if (counter.incrementAndGet() % MODULO == 0) {
                logger.info("Doing commit!");
                indexWriter.commit();
            }
        } catch (IOException e) {
            throw new SearchRuntimeException(String.format("Cannot add book %s to index", url), e);
        }
    }

    public void closeIndexWriter() {
        logger.info("Closing index!");

        try {
            indexWriter.close();
        } catch (IOException e) {
            throw new SearchRuntimeException("Cannot close IndexWriter", e);
        }
    }
}
