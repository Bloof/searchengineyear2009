package ru.ifmo.ctddev.search.crawler;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.apache.log4j.Logger;
import ru.ifmo.ctddev.search.Settings;
import ru.ifmo.ctddev.search.exceptions.SearchRuntimeException;
import ru.ifmo.ctddev.search.index.Indexer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Pattern;

/**
 * Паук, который должен отбирать только страницы с Literature.org.
 * Действует в зависимости от наличия {@link ru.ifmo.ctddev.search.index.Indexer} в {@link ru.ifmo.ctddev.search.Settings}.
 * Либо добавляет книгу в индекс, либо
 * скидывает скачанные файлы в директорию "run/data/" в папке запуска.
 * Для директории должны быть права чтения и записи.
 * <p/>
 * User: Oleg Larionov
 * Date: 28.04.13
 * Time: 18:30
 */
public class LiteratureOrgCrawler extends WebCrawler {

    public static final Pattern LITERATURE_ORG_PATTERN = Pattern.compile(".*literature\\.org.*");
    public static final Pattern HTML_PAGE_PATTERN = Pattern.compile(".*\\.html$");
    public static final String DATA_FOLDER = "run/data/";
    private static Logger logger = Logger.getLogger(LiteratureOrgCrawler.class);
    private File mainDirectory;
    private Indexer indexer;

    public LiteratureOrgCrawler() {
        mainDirectory = new File(DATA_FOLDER);
        mainDirectory.mkdirs();
        indexer = Settings.getInstance().getIndexer();
        checkDirectoryPrivileges(mainDirectory);
    }

    private void checkDirectoryPrivileges(File directory) {
        if (!directory.isDirectory()) {
            throw new SearchRuntimeException("File must be directory!");
        }
        if (!directory.canRead() || !mainDirectory.canWrite()) {
            throw new SearchRuntimeException("Must have permissions to read and write");
        }
    }

    public void setMainDirectory(File mainDirectory) {
        checkDirectoryPrivileges(mainDirectory);
        this.mainDirectory = mainDirectory;
    }

    @Override
    public boolean shouldVisit(WebURL url) {
        String ref = url.getURL().toLowerCase();
        return LITERATURE_ORG_PATTERN.matcher(ref).matches();
    }

    @Override
    public void visit(Page page) {
        WebURL webURL = page.getWebURL();
        String url = webURL.getDomain() + webURL.getPath();
        logger.debug("URL: " + url);

        if (HTML_PAGE_PATTERN.matcher(url).matches()) {
            if (page.getParseData() instanceof HtmlParseData) {
                HtmlParseData htmlData = (HtmlParseData) page.getParseData();
                String text = htmlData.getText().toLowerCase();
                if (indexer != null) {
                    addPageToIndex(url, text);
                }
                writeUrlTextToFile(url, text);
            }
        }
    }

    private void addPageToIndex(String url, String text) {
        indexer.addBook(url, text);
    }

    private void writeUrlTextToFile(String url, String text) {
        File file = touchFile(url);
        logger.info(String.format("Downloading %s to %s", url, file));
        try (PrintWriter pw = new PrintWriter(file)) {
            pw.println(text);
        } catch (FileNotFoundException e) {
            throw new SearchRuntimeException("Cannot write to file " + file.getAbsolutePath(), e);
        }
    }

    private synchronized File touchFile(String url) {
        File newFile = new File(String.format("%s/%s", mainDirectory.getAbsoluteFile(), url));
        logger.debug("Creating file " + newFile.getAbsolutePath());
        newFile.getParentFile().mkdirs();
        try {
            newFile.createNewFile();
            return newFile;
        } catch (IOException e) {
            throw new SearchRuntimeException("Cannot create file " + newFile.getAbsolutePath(), e);
        }
    }
}
