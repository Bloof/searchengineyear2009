package ru.ifmo.ctddev.search;

import ru.ifmo.ctddev.search.index.Indexer;

/**
 * User: Oleg Larionov
 * Date: 29.04.13
 * Time: 13:28
 */
public class Settings {

    private static final Settings INSTANCE = new Settings();
    private Indexer indexer;

    private Settings() {
    }

    public static Settings getInstance() {
        return INSTANCE;
    }

    public Indexer getIndexer() {
        return indexer;
    }

    public void setIndexer(Indexer indexer) {
        this.indexer = indexer;
    }
}
