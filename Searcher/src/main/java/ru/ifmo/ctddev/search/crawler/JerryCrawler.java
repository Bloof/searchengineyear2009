package ru.ifmo.ctddev.search.crawler;


import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: Влад
 * Date: 03.05.13
 * Time: 1:29
 * To change this template use File | Settings | File Templates.
 */
public class JerryCrawler {

    public static final Pattern PATTERN = Pattern.compile("[/]ebooks[/]\\d+");
    public static final String PATH = "http://www.gutenberg.org/files/insert/insert-h/insert-h.htm";

    public static final String DATA = "C:/index/run/gutenberg/";

    public static final String RESOURCE = "http://www.gutenberg.org/ebooks/search/?sort_order=downloads&start_index=";

    public static final long BOOKS = 1001;

    public static final long DELAY = 2000;

    public static final int TIMEOUT = 30000;

    public JerryCrawler() {
    }

    public void download() {
        Document document;
        int page = 1;
        while (page <= BOOKS) {
            try {
                Connection connection = Jsoup.connect(RESOURCE + page);
                Thread.sleep(DELAY);
                connection.timeout(TIMEOUT);
                document = connection.get();
                Elements links = document.select("a[href]");
                for (Element link : links) {
                    if (PATTERN.matcher(link.attr("href")).matches()) {
                        String number = link.attr("href").substring(link.attr("href").lastIndexOf("/") + 1);
                        String location = PATH.replaceAll("insert", number);
                        try{
                            Connection tempConnection = Jsoup.connect(location);
                            Thread.sleep(DELAY);
                            document = tempConnection.get();
                            File file = new File(DATA + number);
                            file.createNewFile();
                            PrintWriter writer = new PrintWriter(file);
                            writer.write(document.text());
                            writer.flush();
                            writer.close();
                            System.out.println(number);
                            Thread.sleep(DELAY);
                        } catch (IOException | InterruptedException ioe){
                            System.out.println(ioe.toString());
                        }
                    }
                }
            } catch (IOException | InterruptedException ioe) {
                System.out.println(ioe.toString());
            }
            page += 25;
        }
    }
}
